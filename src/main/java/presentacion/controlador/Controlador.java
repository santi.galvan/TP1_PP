package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

import configuracion.propiedades.Propiedades;
import modelo.Agenda;
import persistencia.conexion.Conexion;
import presentacion.reportes.ReporteAgenda;
import presentacion.validacion.ValidadorVistaAgregarPersona;
import presentacion.validacion.ValidadorVistaEditarPersona;
import presentacion.vista.VentanaAgregarLocalidad;
import presentacion.vista.VentanaAgregarPersona;
import presentacion.vista.VentanaEditarPersona;
import presentacion.vista.VentanaEditarTipoContacto;
import presentacion.vista.VentanaAgregarTipoContacto;
import presentacion.vista.VentanaConfiguraciónConexion;
import presentacion.vista.VentanaEditarLocalidad;
import presentacion.vista.Vista;
import presentacion.vista.VistaLocalidad;
import presentacion.vista.VistaTipoContacto;
import dto.DatosReporteDTO;
import dto.DireccionDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public class Controlador implements ActionListener, Runnable
{
		private Vista vista;
		
		private List<PersonaDTO> personas_en_tabla;
		private List<DireccionDTO> direcciones_en_tabla;
		private List<LocalidadDTO> localidades_en_tabla;
		private List<TipoContactoDTO> tipoContacto_en_tabla;
	
		private ValidadorVistaAgregarPersona validadorVistaAgregar;
		private ValidadorVistaEditarPersona validadorVistaEditar;
	
		private VentanaAgregarPersona ventanaAgregarPersona;
		private VentanaEditarPersona ventanaEditarPersona; 
		private VentanaAgregarLocalidad ventanaLocalidad;
		private VentanaEditarLocalidad ventanaEditarLocalidad;
		private VentanaEditarTipoContacto ventanaEditarTipoContacto;
		private VentanaAgregarTipoContacto ventanaTipoContacto;
		private VentanaConfiguraciónConexion ventanaConfiguracionConexion;
		
		private VistaLocalidad vistaLocalidad;
		private VistaTipoContacto vistaTipoContacto;
		
		private boolean respuestaVentanaConfiguracion;
		
		private Agenda agenda;
		
		public Controlador(Vista vista, Agenda agenda)
		{
			//ControladorLocalidad controladorLocalidad = new ControladorLocalidad(vista, agenda);
			
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(this);
			this.vista.getBtnBorrar().addActionListener(this);
			this.vista.getBtnEditar().addActionListener(this);
			this.vista.getBtnReporte().addActionListener(this);
			this.vista.getBtnABMLocalidad().addActionListener(this);
			this.vista.getBtnABMTipoContacto().addActionListener(this);
			this.validadorVistaAgregar = new ValidadorVistaAgregarPersona(this);
			this.validadorVistaEditar = new ValidadorVistaEditarPersona(this);
			
			this.agenda = agenda;
			this.personas_en_tabla = null;
			this.direcciones_en_tabla = null;
			this.localidades_en_tabla = null;
			this.tipoContacto_en_tabla = null;
		}
		
		public void inicializar()
		{
			boolean BDLevantada = Conexion.esValida();
			
			while(!BDLevantada) 
			{
				BDLevantada = Conexion.esValida();
				
				 if(this.ventanaConfiguracionConexion == null) {
					 this.ventanaConfiguracionConexion = new VentanaConfiguraciónConexion(this);
					 this.respuestaVentanaConfiguracion = false;
				 }
					
				 while(respuestaVentanaConfiguracion == false)
				 { 
					 System.out.println("Esperando respuesta del usuario...");
					 try
					 {
						Thread.sleep(5000);
					 } 
					 catch (InterruptedException e) 
					 {
						e.printStackTrace();
					 }
				 }
			}
			
			this.llenarTabla();
			this.vista.show();
		}
		
		private void llenarTabla()
		{
			this.vista.getModelPersonas().setRowCount(0); //Para vaciar la tabla
			this.vista.getModelPersonas().setColumnCount(0);
			this.vista.getModelPersonas().setColumnIdentifiers(this.vista.getNombreColumnas());
			
			this.personas_en_tabla = agenda.obtenerPersonas();
			this.direcciones_en_tabla = agenda.obtenerDirecciones();
			this.tipoContacto_en_tabla = agenda.obtenerTipos();
			this.localidades_en_tabla = agenda.obtenerLocalidades(); 
			
			for (int i = 0; i < this.personas_en_tabla.size(); i ++)
			{
				PersonaDTO personaSeleccionada = this.personas_en_tabla.get(i);
				DireccionDTO direccionRelacionada = this.direcciones_en_tabla.get(i);
				
				TipoContactoDTO tipoContactoRelacionado = tipoContactoFiltro(personaSeleccionada.getIdTipoContacto()); 
				LocalidadDTO localidadRelacionada = localidadFiltro(direccionRelacionada.getIdLocalidad());
				
				Object[] fila = {personaSeleccionada.getNombre(), personaSeleccionada.getTelefono(), direccionRelacionada.getCalle(), 
								 direccionRelacionada.getAltura(), direccionRelacionada.getPiso(), direccionRelacionada.getDepartamento(), 
								 localidadRelacionada.getNombre(), personaSeleccionada.getEmail(), personaSeleccionada.getCumpleanios(), tipoContactoRelacionado.getNombre()};
				this.vista.getModelPersonas().addRow(fila);
			}			
		}
		
		// --------------------------------------------------------
		public TipoContactoDTO tipoContactoFiltro(int idTipoContacto) {
			for (int i = 0; i < tipoContacto_en_tabla.size(); i++) {
				if(tipoContacto_en_tabla.get(i).getIdTipoContacto() == idTipoContacto)
					return tipoContacto_en_tabla.get(i);
			}
			return null;
		}
		
		public LocalidadDTO localidadFiltro(int idLocalidad) {
			for (int i = 0; i < tipoContacto_en_tabla.size(); i++) {
				if(localidades_en_tabla.get(i).getIdLocalidad() == idLocalidad)
					return localidades_en_tabla.get(i);
			}
			return null;
		}
		// ----------------------------------------------------------
		
		
		public String[] listarLocalidades()
		{
			this.localidades_en_tabla = agenda.obtenerLocalidades();
			ArrayList<String> mid = new ArrayList<String>();

			for (int i = 0; i < localidades_en_tabla.size(); i++)
			{
				LocalidadDTO localidadSeleccionada = this.localidades_en_tabla.get(i);
				mid.add(localidadSeleccionada.getNombre());
			}
			return mid.toArray(new String[0]);
		}
		
		private void llenarTablaLocalidades()
		{
			this.vistaLocalidad.getModelLocalidades().setRowCount(0); //Para vaciar la tabla
			this.vistaLocalidad.getModelLocalidades().setColumnCount(0);
			this.vistaLocalidad.getModelLocalidades().setColumnIdentifiers(this.vistaLocalidad.getNombreColumnas());
			
			this.localidades_en_tabla = agenda.obtenerLocalidades();
			
			for (int i = 0; i < this.localidades_en_tabla.size(); i ++)
			{
				LocalidadDTO localidadSeleccionada = this.localidades_en_tabla.get(i);
				
				Object[] fila = { localidadSeleccionada.getNombre() };
				this.vistaLocalidad.getModelLocalidades().addRow(fila);
			}			
		}
		
		private void llenarTablaTipoContacto()
		{
			this.vistaTipoContacto.getModelTipoContacto().setRowCount(0); //Para vaciar la tabla
			this.vistaTipoContacto.getModelTipoContacto().setColumnCount(0);
			this.vistaTipoContacto.getModelTipoContacto().setColumnIdentifiers(this.vistaTipoContacto.getNombreColumnas());
			
			this.tipoContacto_en_tabla = agenda.obtenerTipos();
			
			for (int i = 0; i < this.tipoContacto_en_tabla.size(); i ++)
			{
				TipoContactoDTO tipoContactoSeleccionado = this.tipoContacto_en_tabla.get(i);
				
				Object[] fila = { tipoContactoSeleccionado.getNombre() };
				this.vistaTipoContacto.getModelTipoContacto().addRow(fila);
			}			
		}
		
		
		public String[] listarTipos()
		{
			this.tipoContacto_en_tabla = agenda.obtenerTipos();
			ArrayList<String> mid = new ArrayList<String>();

			for (int i = 0; i < tipoContacto_en_tabla.size(); i++)
			{
				TipoContactoDTO tipoContactoSeleccionado = this.tipoContacto_en_tabla.get(i);
				mid.add(tipoContactoSeleccionado.getNombre());
			}
			return mid.toArray(new String[0]);
		}
		
		public void actionPerformed(ActionEvent e) 
		{
			//BOTON CONFIRMAR CONFIGURACION DE BD
			if(this.ventanaConfiguracionConexion != null && e.getSource() == this.ventanaConfiguracionConexion.getBtnConfirmar()) 
			{
				Propiedades.guardarConfiguracionDB(this.ventanaConfiguracionConexion.getTxtUsuario().getText(),
						this.ventanaConfiguracionConexion.getTxtPassword().getText(), this.ventanaConfiguracionConexion.getTxtIp().getText(), this.ventanaConfiguracionConexion.getTxtPuerto().getText());
			
				if(Conexion.esValida()) {
					this.respuestaVentanaConfiguracion = true;
					this.ventanaConfiguracionConexion.dispose();
				}
				else{
					JOptionPane.showMessageDialog(null, "Error al conectarse con la base de datos, verifique datos.");
					return;
				}
			}
			if(this.ventanaConfiguracionConexion != null && e.getSource() == this.ventanaConfiguracionConexion.getBtnSalir()) 
			{
				System.exit(0);
		    }
			//Abrir ventana de Agregar un contacto
			if(e.getSource() == this.vista.getBtnAgregar())
			{
				this.ventanaAgregarPersona = new VentanaAgregarPersona(this);
				this.agenda.addObserver(this.ventanaAgregarPersona);
			}
			//Borra los seleccionados
			else if(e.getSource() == this.vista.getBtnBorrar())
			{
				int[] filas_seleccionadas = this.vista.getTablaPersonas().getSelectedRows();
				for (int fila:filas_seleccionadas)
				{
					this.agenda.borrarPersona(this.personas_en_tabla.get(fila));
					this.agenda.borrarDireccion(this.direcciones_en_tabla.get(fila));
				}
				
				this.llenarTabla();
				
			}
			//Boton para abir la ventana de editar
			else if(e.getSource() == this.vista.getBtnEditar())
			{
				int filaSeleccionada = this.vista.getTablaPersonas().getSelectedRow();
				int idPersona = personas_en_tabla.get(filaSeleccionada).getIdPersona();
	
				this.ventanaEditarPersona = new VentanaEditarPersona(this, idPersona);	
				this.agenda.addObserver(this.ventanaEditarPersona);
			}
			//Genera el reporte
			else if(e.getSource() == this.vista.getBtnReporte())
			{				
				List<PersonaDTO> personas = agenda.obtenerPersonas();
				List<DatosReporteDTO> datosReporte = new ArrayList<DatosReporteDTO>(); 
				HashMap <String,List <DatosReporteDTO>> datosOrdenados = new HashMap<String, List<DatosReporteDTO>>() ; 
			
				for(PersonaDTO persona : personas) 
				{
					DireccionDTO direccionPersona = this.agenda.obtenerDatosDireccion(persona.getIdPersona());
					TipoContactoDTO tipoContacto = this.agenda.obtenerDatosTipoContacto(persona.getIdTipoContacto());
					
					if(!datosOrdenados.containsKey(tipoContacto.getNombre())) {
						List<DatosReporteDTO> datos = new ArrayList<DatosReporteDTO>();
						datosOrdenados.put(tipoContacto.getNombre(), datos);
					}
					
					datosOrdenados.get(tipoContacto.getNombre()).add(new DatosReporteDTO(persona.getNombre(), persona.getTelefono(), direccionPersona.getCalle(), 
							direccionPersona.getAltura(), persona.getEmail(), persona.getCumpleanios(), tipoContacto.getNombre()));
				}

				for (String clave : datosOrdenados.keySet()) {
					Collections.sort(datosOrdenados.get(clave));
					datosReporte.addAll(datosOrdenados.get(clave));
				}
				
				ReporteAgenda reporte = new ReporteAgenda(datosReporte);
				reporte.mostrar();
			}			
			/**VER SI AGREGAR UNA CONDICION QUE SEA != NULL PARA SACAR FACTOR COMÚN**/
			
			/**VENTANAS DE PERSONA**/
			//Boton de agregar tipo de contacto
			else if(this.ventanaAgregarPersona != null && e.getSource() == this.ventanaAgregarPersona.getBtnAgregarTipoContacto())
			{
				this.ventanaTipoContacto = new VentanaAgregarTipoContacto(this);
			}			
			//Boton de confirmar tipo de contacto
			else if(this.ventanaEditarPersona != null && e.getSource() == this.ventanaEditarPersona.getBtnAgregarLocalidad())  
			{
				this.ventanaLocalidad = new VentanaAgregarLocalidad(this);				
			}
			else if (this.ventanaEditarPersona != null && e.getSource() == this.ventanaEditarPersona.getBtnAgregarTipoContacto())
			{
				this.ventanaTipoContacto = new VentanaAgregarTipoContacto(this);

			}	
			else if(this.ventanaAgregarPersona != null && e.getSource() == this.ventanaAgregarPersona.getBtnAgregarLocalidad())
			{
				this.ventanaLocalidad = new VentanaAgregarLocalidad(this);
			}	
			//Boton de confirmacion de agregar contacto
			else if(this.ventanaAgregarPersona != null && e.getSource() == this.ventanaAgregarPersona.getBtnAgregarPersona())
			{
				if(this.validadorVistaAgregar.camposValidosContacto()) {
					int siguienteIdPersona =  this.agenda.obtenerMaxIdPersona()+1;
					int siguienteIdDireccion = this.agenda.obtenerMaxIdDireccion()+1;
					
					int idTipoContacto = this.agenda.obtenerIdTipoContacto( this.ventanaAgregarPersona.getTxtTipoContacto());
					int idLocalidad = this.agenda.obtenerIdLocalidad(this.ventanaAgregarPersona.getTxtLocalidad());
					
					PersonaDTO nuevaPersona = new PersonaDTO(siguienteIdPersona, idTipoContacto, this.ventanaAgregarPersona.getTxtNombre().getText(), 
							ventanaAgregarPersona.getTxtTelefono().getText(), ventanaAgregarPersona.getTxtCumpleanios().getText(),
							ventanaAgregarPersona.getTxtEmail().getText());
									
					DireccionDTO nuevaDireccion = new DireccionDTO(siguienteIdDireccion, siguienteIdPersona, idLocalidad, this.ventanaAgregarPersona.getTxtCalle().getText(), this.ventanaAgregarPersona.getTxtAltura().getText(), 
							this.ventanaAgregarPersona.getTxtPiso().getText(), this.ventanaAgregarPersona.getTxtDepartamento().getText());
					
					this.agenda.agregarPersona(nuevaPersona);
					this.agenda.agregarDireccion(nuevaDireccion);
					
					this.llenarTabla();
					this.ventanaAgregarPersona.dispose();
				}
				else 
				{
					JOptionPane.showMessageDialog(null, validadorVistaAgregar.getMotivoError());
					return;
				}
			}
			
			/** VENTANAS DE LOCALIDAD**/
			//INSERTAR LOCALIDAD
			else if(this.ventanaLocalidad != null && e.getSource() == this.ventanaLocalidad.getBtnConfirmar())
			{
			//	int siguienteIdLocalidad= this.agenda.obtenerMaxIdDireccion()+1;
				
				LocalidadDTO nuevaLocalidad = new LocalidadDTO(0, ventanaLocalidad.getTxtLocalidad().getText());
				this.agenda.agregarLocalidad(nuevaLocalidad);
				
				this.agenda.notifyObserver();
				
				this.ventanaLocalidad.dispose();
			}	
			//ABM TIPO DE CONTACTO
			else if(e.getSource() == this.vista.getBtnABMTipoContacto())
			{
				this.vistaTipoContacto = new VistaTipoContacto(this);
				this.agenda.addObserver(this.vistaTipoContacto);
				llenarTablaTipoContacto();
			}	
			else if(this.vistaTipoContacto != null && e.getSource() == this.vistaTipoContacto.getbtnAgregar())
			{
				this.ventanaTipoContacto = new VentanaAgregarTipoContacto(this);
			}	
			else if(this.vistaTipoContacto != null && e.getSource() == this.vistaTipoContacto.getbtnBorrar())
			{
				int[] filas_seleccionadas = this.vistaTipoContacto.getTablaTipoContacto().getSelectedRows();
				for (int fila:filas_seleccionadas)
				{
					if(!this.agenda.borrarTipoContacto(this.tipoContacto_en_tabla.get(fila)))
						JOptionPane.showMessageDialog(null, "No se puede eliminar, este tipo de contacto tiene una persona asociada");
				}
				
				this.llenarTablaTipoContacto();
			}	
			else if(this.vistaTipoContacto != null && e.getSource() == this.vistaTipoContacto.getbtnEditar())
			{
				int filaSeleccionada = this.vistaTipoContacto.getTablaTipoContacto().getSelectedRow();
				
				if(filaSeleccionada == -1) {
					JOptionPane.showMessageDialog(null, "Debe seleccionar una línea");
					return;
				}
				TipoContactoDTO tipoContactoSeleccionado = this.tipoContacto_en_tabla.get(filaSeleccionada);
				this.ventanaEditarTipoContacto = new VentanaEditarTipoContacto(this, tipoContactoSeleccionado.getNombre());
			}
			else if(this.ventanaEditarTipoContacto != null && e.getSource() == this.ventanaEditarTipoContacto.getBtnConfirmar())
			{
				int filaSeleccionada = this.vistaTipoContacto.getTablaTipoContacto().getSelectedRow()+1;
				TipoContactoDTO tipoContactoEditado = new TipoContactoDTO(filaSeleccionada, this.ventanaEditarTipoContacto.getTxtTipoContacto().getText());
				
				this.agenda.editarTipoContacto(tipoContactoEditado);
				this.agenda.notifyObserver();
				this.ventanaEditarTipoContacto.dispose();
			}
			//ABM LOCALIDAD
			else if(e.getSource() == this.vista.getBtnABMLocalidad())
			{
				this.vistaLocalidad = new VistaLocalidad(this);
				this.agenda.addObserver(this.vistaLocalidad);
				llenarTablaLocalidades();
			}	
			else if(this.vistaLocalidad != null && e.getSource() == this.vistaLocalidad.getbtnAgregar())
			{
				this.ventanaLocalidad = new VentanaAgregarLocalidad(this);
			}	
			else if(this.vistaLocalidad != null && e.getSource() == this.vistaLocalidad.getbtnEditar())
			{
				int filaSeleccionada = this.vistaLocalidad.getTablaLocalidades().getSelectedRow();
				
				if(filaSeleccionada == -1) {
					JOptionPane.showMessageDialog(null, "Debe seleccionar una línea");
					return;
				}
				LocalidadDTO localidadSeleccionada = this.localidades_en_tabla.get(filaSeleccionada);
				this.ventanaEditarLocalidad = new VentanaEditarLocalidad(this, localidadSeleccionada.getNombre());
			}
			else if(this.ventanaEditarLocalidad != null && e.getSource() == this.ventanaEditarLocalidad.getBtnConfirmar())
			{
				int filaSeleccionada = this.vistaLocalidad.getTablaLocalidades().getSelectedRow()+1;
				LocalidadDTO localidadEditada = new LocalidadDTO(filaSeleccionada, this.ventanaEditarLocalidad.getTxtLocalidad().getText());
				
				this.agenda.editarLocalidad(localidadEditada);
				this.agenda.notifyObserver();
				this.ventanaEditarLocalidad.dispose();
			}	
			
			else if(this.vistaLocalidad != null && e.getSource() == this.vistaLocalidad.getbtnBorrar())
			{
				int[] filas_seleccionadas = this.vistaLocalidad.getTablaLocalidades().getSelectedRows();
				for (int fila:filas_seleccionadas)
				{
					if(!this.agenda.borrarLocalidad(this.localidades_en_tabla.get(fila)))
						JOptionPane.showMessageDialog(null, "Error: Localidad asociada a una persona.");
				}
				
				this.llenarTablaLocalidades();
			}	
			//EDITAR CONTACTO
			else if(this.ventanaTipoContacto != null && e.getSource() == this.ventanaTipoContacto.getBtnConfirmar())
			{
				TipoContactoDTO nuevoTipo = new TipoContactoDTO(0, ventanaTipoContacto.getTxtTipoContacto().getText());	
				this.agenda.agregarTipoContacto(nuevoTipo);
				
				this.agenda.notifyObserver();

				this.ventanaTipoContacto.dispose();
			}
			
			//Boton para confirmar la edicion de la persona
			else if(this.ventanaEditarPersona != null && e.getSource() == this.ventanaEditarPersona.getBtnConfirmarEditar())
			{
				if(this.validadorVistaEditar.camposValidosContacto()) 
				{
					int idLocalidad = this.agenda.obtenerIdLocalidad(this.ventanaEditarPersona.getTxtLocalidad());
					int idTipoContacto = this.agenda.obtenerIdTipoContacto(this.ventanaEditarPersona.getTxtTipoContacto());
					
					int filaSeleccionada = this.vista.getTablaPersonas().getSelectedRow();
					
					int idPersona = personas_en_tabla.get(filaSeleccionada).getIdPersona();
					
					int idDireccion = direcciones_en_tabla.get(filaSeleccionada).getIdDireccion();
					
					PersonaDTO personaEditada = new PersonaDTO(idPersona, idTipoContacto, this.ventanaEditarPersona.getTxtNombre().getText(), 
							ventanaEditarPersona.getTxtTelefono().getText(), ventanaEditarPersona.getTxtCumpleanios().getText(),
							ventanaEditarPersona.getTxtEmail().getText());
					
					DireccionDTO direccionEditada = new DireccionDTO(idDireccion, idPersona, idLocalidad, this.ventanaEditarPersona.getTxtCalle().getText(),
							this.ventanaEditarPersona.getTxtAltura().getText(), this.ventanaEditarPersona.getTxtPiso().getText(), this.ventanaEditarPersona.getTxtDepartamento().getText());
					
					TipoContactoDTO tipoContactoEditado = new TipoContactoDTO(idTipoContacto, this.ventanaEditarPersona.getTxtTipoContacto()); 
					
					this.agenda.editarPersona(personaEditada);
					this.agenda.editarTipoContacto(tipoContactoEditado);
					this.agenda.editarDireccion(direccionEditada);
					
					this.llenarTabla();
					this.ventanaEditarPersona.dispose();
				}
				else 
				{
					JOptionPane.showMessageDialog(null, validadorVistaEditar.getMotivoError());
					return;
				}
			}	
		}

		public String[] obtenerDatosPersona(int idPersona) 
		{
			PersonaDTO persona = this.obtenerPersona(idPersona);
			DireccionDTO direccion = this.obtenerDireccion(idPersona);
			TipoContactoDTO tipoContacto = this.obtenerTipoContacto(persona.getIdTipoContacto());
			LocalidadDTO localidad = this.obtenerLocalidad(direccion.getIdLocalidad());
			
			return new String[] {persona.getNombre(), persona.getTelefono(), direccion.getCalle(), direccion.getAltura(), 
				direccion.getPiso(), direccion.getDepartamento(), localidad.getNombre(), persona.getEmail(), persona.getCumpleanios(), tipoContacto.getNombre()};
		}
		// ----------------------------------------------
		public PersonaDTO obtenerPersona(int idPersona) {
			for (int i = 0; i < personas_en_tabla.size(); i++) {
				if(personas_en_tabla.get(i).getIdPersona() == idPersona)
						return personas_en_tabla.get(i);
			}
			return null;
		}
		
		public DireccionDTO obtenerDireccion(int idPersona) {
			for (int i = 0; i < direcciones_en_tabla.size(); i++) {
				if(direcciones_en_tabla.get(i).getIdPersona() == idPersona)
						return direcciones_en_tabla.get(i);
			}
			return null;
		}
		
		public TipoContactoDTO obtenerTipoContacto(int idTipoContacto) {
			for (int i = 0; i < tipoContacto_en_tabla.size(); i++) {
				if(tipoContacto_en_tabla.get(i).getIdTipoContacto() == idTipoContacto)
						return tipoContacto_en_tabla.get(i);
			}
			return null;
		}
		
		public LocalidadDTO obtenerLocalidad(int idLocalidad) {
			for (int i = 0; i < localidades_en_tabla.size(); i++) {
				if(localidades_en_tabla.get(i).getIdLocalidad() == idLocalidad)
						return localidades_en_tabla.get(i);
			}
			return null;
		}

		// -------------------------
		public Vista getVista() 
		{
			return this.vista;
		}
		
		public VentanaAgregarPersona getVentanaAgregarPersona() 
		{
			return this.ventanaAgregarPersona;
		}

		public void refrescarTipoContactos() 
		{
			this.llenarTablaTipoContacto();
		}

		public void refrescarLocalidades() 
		{
			this.llenarTablaLocalidades();			
		}

		public VentanaEditarPersona getVentanaEditarPersona() {
			return this.ventanaEditarPersona;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
		}
		
}
