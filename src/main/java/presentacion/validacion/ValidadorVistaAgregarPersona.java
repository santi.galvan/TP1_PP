package presentacion.validacion;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import presentacion.controlador.Controlador;

public class ValidadorVistaAgregarPersona 
{
	private Controlador controlador;
	private String motivoError;
	private int contadorErrores;
	
	public ValidadorVistaAgregarPersona(Controlador controlador) 
	{
		this.controlador = controlador;
		this.motivoError = "";
	}
	
	public boolean camposValidosContacto() 
	{
		this.motivoError = "Por favor verifique el campo: ";
		this.contadorErrores = 0;
		
		this.campoNombreVacio();
		this.campoTelefonoVacio();
		this.campoLocalidadVacio();
		this.campoTipoContactoVacio();
		this.fechaCumpleaniosValida();

		return (contadorErrores > 0) ? false : true;
	}

	private void campoNombreVacio() 
	{
		String textoCampoNombre =  this.controlador.getVentanaAgregarPersona().getTxtNombre().getText();
	
		if(textoCampoNombre.length() == 0) 
		{
			motivoError = motivoError + "Nombre, ";
			contadorErrores++;
		}
	}

	private void campoTipoContactoVacio() 
	{
		String textoTipo = controlador.getVentanaAgregarPersona().getTxtTipoContacto();
		if(textoTipo.length() == 0 || textoTipo.equals("<Tipo de Contacto>")) 
		{
			motivoError = motivoError + "Tipo de contacto, ";
			contadorErrores++;
		}
	}

	private void campoLocalidadVacio() 
	{
		String textoLocalidad = controlador.getVentanaAgregarPersona().getTxtLocalidad();
		if(textoLocalidad.length() == 0 || textoLocalidad.equals("<Localidad>")) 
		{
			motivoError = motivoError + "Localidad, ";
			contadorErrores++;
		}
	}

	private void campoTelefonoVacio() 
	{
		String textoCampoTelefono =  this.controlador.getVentanaAgregarPersona().getTxtTelefono().getText();
		if(textoCampoTelefono.length() == 0) 
		{
			motivoError = motivoError + "Telefono, ";
			contadorErrores++;
		}
	}
	
	private void fechaCumpleaniosValida() 
	{
		String fechaIngresada = this.controlador.getVentanaAgregarPersona().getTxtCumpleanios().getText();
		
		if(fechaIngresada.length() != 10)
		{ 
	    	motivoError = motivoError + "Fecha de cumpleaños(Formato DD/MM/AAAA).";
	    	contadorErrores++;
	    	return; 
        }
		
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setLenient(false);
            dateFormat.parse(fechaIngresada);
            return;
        } 
        catch (ParseException e)
        { 
        	motivoError = motivoError + "Fecha de cumpleaños(Fecha invalida).";
        	contadorErrores++;
        	return; 
        }
	}

	public String getMotivoError() 
	{
		return this.motivoError;
	}
	
}
	
	