package presentacion.vista;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import observer.Observer;
import presentacion.controlador.Controlador;
import javax.swing.JLabel;

public class VistaTipoContacto extends JFrame implements Observer
{	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTable tablaTipoContacto;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private DefaultTableModel modelTipoContacto;
	private  String[] nombreColumnas = {"Tipo de contacto" };

	private Controlador controlador;

	public VistaTipoContacto(Controlador controlador) 
	{
		super();
		setTitle("ABM - Tipo de contacto");
		this.controlador = controlador;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 290, 432);
		this.setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		modelTipoContacto = new DefaultTableModel(null, nombreColumnas);
		
		btnAgregar = new JButton("");
		btnAgregar.setBounds(21, 351, 50, 50);
		btnAgregar.setIcon(new ImageIcon(Vista.class.getResource("/img/AgregarTipoPersona.png")));
		btnAgregar.setContentAreaFilled(false);
		btnAgregar.setBorderPainted(false);		contentPane.add(btnAgregar);
		btnAgregar.addActionListener(this.controlador);

		btnBorrar = new JButton("");
		btnBorrar.setBounds(222, 351, 50, 50);
		btnBorrar.setIcon(new ImageIcon(Vista.class.getResource("/img/BorrarTipoPersona.png")));
		btnBorrar.setContentAreaFilled(false);
		btnBorrar.setBorderPainted(false);
		contentPane.add(btnBorrar);
		btnBorrar.addActionListener(this.controlador);
		
		btnEditar = new JButton("");
		btnEditar.setBounds(118, 351, 50, 50);
		btnEditar.setIcon(new ImageIcon(Vista.class.getResource("/img/editarTipoPersona.png")));
		btnEditar.setContentAreaFilled(false);
		btnEditar.setBorderPainted(false);
		contentPane.add(btnEditar);
		btnEditar.addActionListener(this.controlador);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(21, 34, 251, 306);
		contentPane.add(spPersonas);
		tablaTipoContacto = new JTable(modelTipoContacto); 
		
		tablaTipoContacto.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaTipoContacto.getColumnModel().getColumn(0).setResizable(false);
		
		spPersonas.setViewportView(tablaTipoContacto);
		
		JLabel lblLocalidades = new JLabel("Contactos");
		lblLocalidades.setBounds(118, 0, 81, 14);
		contentPane.add(lblLocalidades);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon(Vista.class.getResource("/img/diseñoEdit.png")));
		lblFondo.setBounds(0, 0, 294, 407);
		contentPane.add(lblFondo);
		this.setVisible(true);
	}
	
	public JButton getbtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getbtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getbtnEditar() 
	{
		return btnEditar;
	}
	
	public DefaultTableModel getModelTipoContacto() 
	{
		return modelTipoContacto;
	}
	
	public JTable getTablaTipoContacto()
	{
		return tablaTipoContacto;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	@Override
	public void update()
	{
		this.controlador.refrescarTipoContactos();
	}

}

