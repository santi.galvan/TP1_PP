package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DireccionDAO;
import dto.DireccionDTO;

public class DireccionDAOSQL implements DireccionDAO
{
	private static final String insert = "INSERT INTO direccion(idPersona, idDireccion, idLocalidad, calle, altura, piso, departamento) VALUES(?, DEFAULT, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE direccion SET idLocalidad = ?, calle = ?, altura = ?, piso = ?, departamento = ? WHERE idDireccion = ?";
	private static final String selectDireccion = "SELECT * FROM direccion WHERE idPersona = ?";
	private static final String max = "SELECT IFNULL(MAX(idDireccion), 0) AS resultado FROM direccion";
	private static final String delete = "DELETE FROM direccion WHERE idDireccion = ?";
	private static final String readall = "SELECT * FROM direccion";
	private static final String exists = "SELECT EXISTS (SELECT idLocalidad FROM direccion where idLocalidad = ?) as resultado";
	
	
	public boolean insert(DireccionDTO direccion)
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, direccion.getIdPersona());
			statement.setInt(2, direccion.getIdLocalidad());	
			statement.setString(3, direccion.getCalle());
			statement.setString(4, direccion.getAltura());
			statement.setString(5, direccion.getPiso());
			statement.setString(6, direccion.getDepartamento());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean update(DireccionDTO direccion)
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setInt(1, direccion.getIdLocalidad());
			statement.setString(2, direccion.getCalle());
			statement.setString(3, direccion.getAltura());
			statement.setString(4, direccion.getPiso());
			statement.setString(5, direccion.getDepartamento());
			statement.setInt(6, direccion.getIdDireccion());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

	public DireccionDTO selectDireccion(int idPersona) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(selectDireccion);
			statement.setInt(1, idPersona);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) 
			{
				return new DireccionDTO(resultSet.getInt("idPersona"), resultSet.getInt("idDireccion"), resultSet.getInt("idLocalidad"),
						resultSet.getString("Calle"), resultSet.getString("Altura"), resultSet.getString("Piso"), resultSet.getString("Departamento"));		
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public Integer max()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(max);
			resultSet = statement.executeQuery();
		
			if(resultSet.next())
				return resultSet.getInt("resultado");
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return -1;
	}
	
	public boolean delete(DireccionDTO direccion_a_eliminar)
	{
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, Integer.toString(direccion_a_eliminar.getIdDireccion()));
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean asociadaLocalidad(int idLocalidad) {
		PreparedStatement statement;
		ResultSet resultSet ; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(exists);
			statement.setInt(1, idLocalidad);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) {
				System.out.println("Exists: "+ resultSet.getInt("resultado"));
				if(resultSet.getInt("resultado") == 1) {
					return true;	
				}
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public List<DireccionDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<DireccionDTO> direcciones = new ArrayList<DireccionDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				direcciones.add(new DireccionDTO(resultSet.getInt("idPersona"), resultSet.getInt("IdDireccion"), resultSet.getInt("idLocalidad"), 
						resultSet.getString("Calle"), resultSet.getString("Altura"), resultSet.getString("Piso"), resultSet.getString("Departamento")));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return direcciones;
	}
}
