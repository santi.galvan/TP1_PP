package persistencia.dao.interfaz;

import java.util.List;

import dto.DireccionDTO;

public interface DireccionDAO 
{
	public boolean insert(DireccionDTO direccion);
	
	public boolean update(DireccionDTO direccion);

	public boolean delete(DireccionDTO direccion_a_eliminar);
	
	public DireccionDTO selectDireccion(int idPersona);
	
	public Integer max();
	
	public List<DireccionDTO> readAll();

	public boolean asociadaLocalidad(int idLocalidad);
}

