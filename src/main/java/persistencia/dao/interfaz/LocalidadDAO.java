package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;

public interface LocalidadDAO 
{
	public boolean insert(LocalidadDTO direccion);
	
	public boolean update(LocalidadDTO direccion);

	public boolean delete(LocalidadDTO direccion_a_eliminar);

	public List<LocalidadDTO> readAll();

	public LocalidadDTO selectLocalidad(int idLocalidad);

	public int selectIdLocalidad(String txtLocalidad);
}

