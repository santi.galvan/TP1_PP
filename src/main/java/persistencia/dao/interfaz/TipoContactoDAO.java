package persistencia.dao.interfaz;

import java.util.List;

import dto.TipoContactoDTO;

public interface TipoContactoDAO 
{
	public boolean insert(TipoContactoDTO tipoContacto);
	
	public boolean update(TipoContactoDTO tipoContacto);

	public boolean delete(TipoContactoDTO tipoContacto_a_eleminar);
	
	public List<TipoContactoDTO> readAll();

	public TipoContactoDTO selectTipoContacto(int idTipoContacto);

	public int selectIdTipoContacto(String tipoContacto);
}

