package modelo;

import java.util.ArrayList;
import java.util.List;

import dto.PersonaDTO;
import dto.TipoContactoDTO;
import dto.DireccionDTO;
import dto.LocalidadDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DireccionDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;
import observer.Observable;
import observer.Observer;


public class Agenda implements Observable
{
	private PersonaDAO persona;	
	private DireccionDAO direccion;
	private LocalidadDAO localidad;
	private TipoContactoDAO tipoContacto;
	//private List<String> localidades = new ArrayList<>(Arrays.asList("<Localidad>", "Grand Bourg", "San Miguel", "Pilar", "Don Torcuato",  "Palermo", "Avellaneda", "Capital Federal", "José C. Paz"));
	
	private List<Observer> observers = new ArrayList<Observer>();
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.direccion = metodo_persistencia.createDireccionDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.tipoContacto = metodo_persistencia.createTipoContactoDAO();
	}
	
	//Persona
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}
	
	public PersonaDTO obtenerDatosPersona(int idPersona) 
	{
		return this.persona.selectPersona(idPersona);
	}
	
	public void editarPersona(PersonaDTO persona) 
	{
		this.persona.update(persona);
	}
	
	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}

	public Integer obtenerMaxIdPersona() {
		return this.persona.max();
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();			
	}
	//Direccion
	public void agregarDireccion(DireccionDTO nuevaDireccion)
	{
		this.direccion.insert(nuevaDireccion);
	}
	
	public void editarDireccion(DireccionDTO direccionEditada)
	{
		this.direccion.update(direccionEditada);
	}
	
	public void borrarDireccion(DireccionDTO direccion_a_eliminar) 
	{
		this.direccion.delete(direccion_a_eliminar);
	}
	
	public DireccionDTO obtenerDatosDireccion(int idPersona) 
	{
		return this.direccion.selectDireccion(idPersona);
	}
	
	public Integer obtenerMaxIdDireccion() {
		return this.direccion.max();
	}
	
	public List<DireccionDTO> obtenerDirecciones()
	{
		return this.direccion.readAll();		
	}
	//Localidad
	public void agregarLocalidad(LocalidadDTO localidad) 
	{
		this.localidad.insert(localidad);
	}
	
	public void editarLocalidad(LocalidadDTO localidadEditada) {
		this.localidad.update(localidadEditada);
	}
	
	public boolean borrarLocalidad(LocalidadDTO localidad) 
	{
		if(!this.direccion.asociadaLocalidad(localidad.getIdLocalidad()))
		{
			this.localidad.delete(localidad);
			return true;			
		}
		return false;
	}
	
	public LocalidadDTO obtenerDatosLocalidad(int idLocalidad) 
	{
		return this.localidad.selectLocalidad(idLocalidad);
	}
	
	public List<LocalidadDTO> obtenerLocalidades() 
	{
		return this.localidad.readAll();
	}
	
   public int obtenerIdLocalidad(String txtLocalidad) {
	   // TODO Auto-generated method stub
	   return this.localidad.selectIdLocalidad(txtLocalidad);
   }
	   
	//Tipo de contacto
   public int obtenerIdTipoContacto(String txtTipoContacto) {
	   return this.tipoContacto.selectIdTipoContacto(txtTipoContacto);
   }
	
	public TipoContactoDTO obtenerDatosTipoContacto(int idTipoContacto) 
	{
		return this.tipoContacto.selectTipoContacto(idTipoContacto);
	}
	
	public void agregarTipoContacto(TipoContactoDTO tipoContacto) 
	{
		this.tipoContacto.insert(tipoContacto);
	}
	
	
	public void editarTipoContacto(TipoContactoDTO tipoContactoEditado) 
	{
		this.tipoContacto.update(tipoContactoEditado);
	}
		
	public boolean borrarTipoContacto(TipoContactoDTO tipoContacto) 
	{
		if(!this.persona.asociadoTipoContacto(tipoContacto.getIdTipoContacto())) {
			this.tipoContacto.delete(tipoContacto);
			return true;
		}
		return false;
	}
	
	
	public List<TipoContactoDTO> obtenerTipos() 
	{
		return this.tipoContacto.readAll();
	}

	//METODOS DE OBSERVER PATTERN
   public void addObserver(Observer observer)
   {
	   observers.add(observer);	
   }
   
   public void removeObserver(Observer observer)
   {
	   observers.remove(observer);
   }
   
   public void notifyObserver()
   {
	   for(Observer obs: observers) 
	   {
		   if(obs != null) 
			   obs.update();
		   
	   }
   }
}
